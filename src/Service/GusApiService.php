<?php

declare(strict_types=1);

namespace XOne\Bundle\GusBundle\Service;

use GusApi\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use XOne\Bundle\GusBundle\Exception\InvalidVatId;
use XOne\Bundle\GusBundle\Provider\GusClientProvider;

readonly class GusApiService
{
    public function __construct(
        protected TranslatorInterface $translator,
        protected GusClientProvider   $gusClientProvider,
        protected string              $gusToken
    ) {
    }

    /**
     * @throws NotFoundException
     */
    public function search(string $nip): JsonResponse
    {
        try {
            $this->validateNip($nip);
            $response = $this->gusClientProvider->createClient()->getByNip($nip);
            $success = true;
        } catch (\Exception $e) {
            $response = [
                'message' => $this->translator->trans($e->getMessage()),
            ];
            $success = false;
        }

        return new JsonResponse([
                'success' => $success,
                'data'    => $response
            ]);
    }

    /**
     * @throws InvalidVatId
     */
    private function validateNip(string $nip):bool
    {
        $nipWithoutDashes = preg_replace("/-/", "", $nip);
        $reg = '/^[0-9]{10}$/';
        if (!preg_match($reg, $nipWithoutDashes)) {
            throw new InvalidVatId();
        } else {
            $digits = str_split($nipWithoutDashes);
            $checksum = (6*intval($digits[0]) + 5*intval($digits[1]) + 7*intval($digits[2]) + 2*intval($digits[3]) + 3*intval($digits[4]) + 4*intval($digits[5]) + 5*intval($digits[6]) + 6*intval($digits[7]) + 7*intval($digits[8]))%11;

            return (intval($digits[9]) == $checksum);
        }
    }
}
