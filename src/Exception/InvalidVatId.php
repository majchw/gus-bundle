<?php

declare(strict_types=1);

namespace XOne\Bundle\GusBundle\Exception;

use Exception;

class InvalidVatId extends Exception
{
    public function __construct(string $message = 'Invalid VAT ID')
    {
        parent::__construct($message);
    }
}