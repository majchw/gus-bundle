<?php

declare(strict_types=1);

namespace XOne\Bundle\GusBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use XOne\Bundle\GusBundle\Service\GusApiService;

#[Route('/gus',name: 'gus_')]
class GusController extends AbstractController
{
    public function __construct(
        protected GusApiService $gusApiService
    ) {
    }

    #[Route('/search/{nip}', name: 'search', methods: ['GET'])]
    public function searchByNip(string $nip): JsonResponse
    {
        return $this->gusApiService->search($nip);
    }
}