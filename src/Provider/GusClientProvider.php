<?php

declare(strict_types=1);

namespace XOne\Bundle\GusBundle\Provider;

use GusApi\GusApi;

class GusClientProvider {
    public function __construct(
        private string $gusToken
    ) {
    }

    public function createClient (): GusApi
    {
        $client = new GusApi(userKey: $this->gusToken);

        $client->login();

        return $client;
    }
}
