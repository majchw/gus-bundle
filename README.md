1. Instalacja

Dodaj do swojego dockerfile i przebuduj kontener:

```bash
docker-php-ext-install soap
docker-php-ext-install simplexml
```

W kontenerze uruchom:

```bash
composer require xone/gus-bundle
```

Dodaj do pliku .env:

```dotenv
GUS_TOKEN=klucz_api
```

Klucz produkcyjny można uzyskać składając wniosek na https://api.stat.gov.pl/Home/RegonApi
Klucz testowy dostępny w passbolcie (gustoken)

Dodaj plik konfiguracyjny:

```yaml
# config/routes/x_one_gus.yaml
x_one_gus:
  resource: "@XOneGusBundle/config/routes.yaml"
```

2. Użycie

Został wystawiony endpoint /gus/search, który przyjmuje parametr NIP. W odpowiedzi zwraca dane firmy z GUS.

W przypadku użycia w innej klasie:

```php
use XOne\Bundle\GusBundle\Service\GusApiService;

class YourService
{
    public function __construct(protected GusApiService $gusApiService)
    {}

    public function getCompanyData(string $nip)
    {
        $companyData = $this->gusApiService->search($nip);
    }
}
```

W GusApiService numer NIP jest walidowany, jeśli nie jest poprawny zostanie rzucony wyjątek.
   
